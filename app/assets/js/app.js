document.addEventListener('DOMContentLoaded',function(){
    const FORM = document.getElementById('formConsultUser'), AVATAR = document.getElementById('userAvatar'),
          FNAME = document.getElementById('fullName'), BIO = document.getElementById('bio'),
          EMAIL = document.getElementById('email'), BODYCARD = document.getElementById('withUser');
    if (FORM){  // If exists
        FORM.addEventListener('submit',(e) => {
            let name = document.querySelector('input[name=username]').value; // Getting value of input 
            expReg = /\s/g; // Regular expresion
            name = name.replace(expReg,"");  // Deleting spaces blank
            // Consult
            fetch(`https://api.github.com/search/users?q=user:${name}`)
            .then(response => {
                return response.status === 200 ?  response.json() : false; // If response is correct, return data, else return false
            }).then(response => {
                response && response.items.length > 0 ? userData(response.items[0]) : userUnknown('User does not exist') // If is correct, search details, else show erro notice 
            })
            e.preventDefault();
        })
    }
    
    // start : functions
    const userData = data => {
        console.log(data)
        fetch(data.url).then(response => {
            return response.status  === 200 ? response.json() : false
        }).then(response => {
            // Getting repositories
            fetch(response.repos_url).then(response_repos => {
                if (response_repos.status === 200){
                    // Change data profile 
                    AVATAR.setAttribute('src',response.avatar_url);
                    FNAME.textContent = response.name;
                    BIO.textContent = response.bio;
                    EMAIL.textContent = response.email;
                    return response_repos.json()
                }else{userUnknown('Error to get repositories');}
            }).then(result => {
                result ? userRepos(result) : console.error('Fail to get repositories')
            })
        })
    }
    
    const userRepos = data => {
        document.getElementById('error').style.display = 'none'; // Hidden element of error notice
        console.log(data)
        document.getElementById('reposCount').textContent = data.length
        // Modified styles of the card 
        card = document.querySelector('div[class=card]'); // Getting card
        if (card.getAttribute('data-state') == 'false'){    // If card is small size
            card.style.minHeight = '78vh';
            card.style.transition = 'min-height .4s';
            card.setAttribute('data-state',true);
        }
        document.getElementById('repositories').innerHTML = ''; // Empty container repositories
        // --
        mainContainer = document.getElementById('repositories');
        fragment = document.createDocumentFragment()    // Fragment for save all components
        // Getting each repositorie 
        for (repo = 0; repo <= data.length - 1; repo ++){
            fragment.appendChild(component(data[repo])) // Saving components
        }
        mainContainer.append(fragment)  // Adding components to the view
        if (BODYCARD.getAttribute('data-display') == 'false') {BODYCARD.setAttribute('data-display',true) } // State body section for see
        BODYCARD.getAttribute('data-display') == 'true' ? BODYCARD.style.display = 'block' : BODYCARD.style.display = 'none'
    }

    const userUnknown = message => {
        // Modified styles card 
        card = document.querySelector('div[class=card]');
        card.style.minHeight = '25vh';
        card.style.transition = 'min-height .4s';
        card.setAttribute('data-state',false) 
        // display element card 
        document.getElementById('withUser').style.display = 'none';
        document.getElementById('MessageNotice').textContent = message
        setTimeout(() => {
            document.getElementById('error').style.display = 'block'; // Display error notice
        },500)
    }

    const component = data  => {
        // -- Creating elements 
        var divWrapper = document.createElement('div'), divTitle = document.createElement('div'), 
            divDetails = document.createElement('div'), hTitle = document.createElement('h4');
        // -- Adding classes 
        divWrapper.classList.add('wrapper-repo'); divTitle.classList.add('repoTitle');
        divDetails.classList.add('details-repo'); hTitle.classList.add('repo-title');
        // -- Adding title
        hTitle.textContent = `${data.name}`;
        // -- Elements second section
        let divDetailStarts = document.createElement('div'), divDetailForks = document.createElement('div'), 
            spanStart = document.createElement('span'),spanFork = document.createElement('span'), // Span elements
            iconStart = document.createElement('i'),iconFork = document.createElement('i'); // Icons
        // -- Adding classes
        iconStart.classList.add('fas','fa-star'); iconFork.classList.add('fas','fa-code-branch');
        divDetailStarts.classList.add('details'); divDetailForks.classList.add('details')
        // -- Adding details
        spanStart.textContent = `${data.stargazers_count}`; spanFork.textContent = `${data.forks_count}`

        // Assembling componente
        divTitle.append(hTitle);
        divDetailStarts.appendChild(iconStart); divDetailStarts.appendChild(spanStart);
        divDetailForks.appendChild(iconFork); divDetailForks.appendChild(spanFork);
        divDetails.appendChild(divDetailStarts); divDetails.appendChild(divDetailForks);
        divWrapper.appendChild(divTitle); divWrapper.appendChild(divDetails);
        // --
        return divWrapper; // Componente complete
    }
    // end : functions 
})

